import { useState, useEffect } from 'react';
import { Person } from 'components/PersonInfo/PersonInfo';

import apiData from './api';

const useFetchContacts = () => {
  const [reloads, setReloads] = useState<number>(0);
  const [data, setData] = useState<Person[] | null>(null);
  const [hasError, setHasError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const reload = () => setReloads(reloads + 1);

  useEffect(() => {
    setIsLoading(true);
    apiData()
      .then((response: Person[]) => {
        setData(response);
        setHasError(false);
      })
      .catch(() => setHasError(true))
      .finally(() => setIsLoading(false));
  }, [reloads]);

  return {
    data,
    isLoading,
    hasError,
    reload,
  };
};

export default useFetchContacts;
