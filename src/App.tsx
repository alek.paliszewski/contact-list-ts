import ContactList from 'views/ContactList/ContactList';

function App() {
  return (
    <ContactList />
  );
}

export default App;
