import { useEffect, useState } from 'react';
import PersonInfo, { Person } from 'components/PersonInfo/PersonInfo';
import Spinner from 'components/Spinner/Spinner';
import Button from 'components/Button/Button';
import useFetchContacts from 'utils/useFetchContacts';
import './ContactList.css';

function ContactList() {
  const [selected, setSelected] = useState<Person[]>([]);
  const [unselected, setUnselected] = useState<Person[]>([]);

  const {
    data,
    isLoading,
    hasError,
    reload,
  } = useFetchContacts();

  useEffect(() => {
    if (data) {
      setUnselected((p) => p.concat(data));
    }
  }, [data]);

  const handleSelect = (person: Person) => {
    if (selected.includes(person)) {
      setSelected((arr) => arr.filter((p) => p !== person));
      setUnselected((arr) => [person, ...arr]);
    } else {
      setUnselected((arr) => arr.filter((p) => p !== person));
      setSelected((arr) => [person, ...arr]);
    }
  };

  if (isLoading && !unselected.length && !selected.length) return <Spinner />;

  return (
    <div className="contacts">
      <div className="contacts__counter">
        Selected contacts:
        {' '}
        {selected.length}
      </div>
      <div className="contacts__list">
        {selected.map((p) => (
          <PersonInfo key={p.id} data={p} onClick={() => handleSelect(p)} selected />
        ))}
        {unselected.map((p) => (
          <PersonInfo key={p.id} data={p} onClick={() => handleSelect(p)} selected={false} />
        ))}
        <Button onClick={reload} isLoading={isLoading} hasError={hasError} />
      </div>

    </div>
  );
}

export default ContactList;
