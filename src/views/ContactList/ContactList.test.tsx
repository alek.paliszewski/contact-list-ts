import {
  render, screen, fireEvent,
} from '@testing-library/react';
import * as apiCall from 'utils/api';
import ContactList from './ContactList';

jest.mock('utils/api');

const contactsBatch = [{
  id: '1',
  jobTitle: 'Fabricator',
  emailAddress: 'Ron_Giles3711@dionrab.com',
  firstNameLastName: 'Ron Giles',
},
{
  id: '2',
  jobTitle: 'IT Support Staff',
  emailAddress: 'Melinda_Mcgregor7556@mafthy.com',
  firstNameLastName: 'Melinda Mcgregor',
}];

const contactsNextBatch = [{
  id: '3',
  jobTitle: 'Call Center Representative',
  emailAddress: 'Wade_Steer2239@cispeto.com',
  firstNameLastName: 'Wade Steer',
},
{
  id: '4',
  jobTitle: 'Budget Analyst',
  emailAddress: 'Alexander_Woodley4471@infotech44.tech',
  firstNameLastName: 'Alexander Woodley',
}];

test('display contacts and loads more', async () => {
  jest.spyOn(
    apiCall,
    'default',
  ).mockImplementationOnce(() => Promise.resolve(contactsBatch))
    .mockImplementationOnce(() => Promise.resolve(contactsNextBatch));

  render(<ContactList />);

  expect(await screen.findAllByTestId('person-info')).toHaveLength(2);

  fireEvent.click(screen.getByText('Load more'));

  expect(await screen.findByText('Loading...')).toBeInTheDocument();

  expect(await screen.findAllByTestId('person-info')).toHaveLength(4);
});

test('display fetching error and successfully loads more', async () => {
  jest.spyOn(
    apiCall,
    'default',
  ).mockImplementationOnce(() => Promise.reject())
    .mockImplementationOnce(() => Promise.resolve(contactsBatch));

  render(<ContactList />);

  const button = await screen.findByText(/We had a problem, try again/);

  expect(button).toBeInTheDocument();

  fireEvent.click(button);

  expect(await screen.findByText('Loading...')).toBeInTheDocument();

  expect(await screen.findAllByTestId('person-info')).toHaveLength(2);
});

test('selecting and unselecting contacts', async () => {
  jest.spyOn(
    apiCall,
    'default',
  ).mockResolvedValue(contactsBatch);

  render(<ContactList />);

  const contacts = await screen.findAllByTestId('person-info');

  expect(contacts).toHaveLength(2);

  fireEvent.click(contacts[0]);

  expect(await screen.findByText(/Selected contacts: 1/)).toBeInTheDocument();

  fireEvent.click(contacts[1]);

  expect(await screen.findByText(/Selected contacts: 2/)).toBeInTheDocument();

  const selectedContacts = await screen.findAllByTestId('person-info');

  fireEvent.click(selectedContacts[1]);

  expect(await screen.findByText(/Selected contacts: 1/)).toBeInTheDocument();

  fireEvent.click(selectedContacts[0]);

  expect(await screen.findByText(/Selected contacts: 0/)).toBeInTheDocument();
});
