import './Button.css';

type Props = {isLoading: boolean, hasError: boolean, onClick: () => void}

export default function Button({ isLoading, hasError, onClick }: Props) {
  return (
    <button type="button" className="button" onClick={onClick} disabled={isLoading}>
      {hasError ? 'We had a problem, try again' : isLoading ? 'Loading...' : 'Load more'}
    </button>
  );
}
