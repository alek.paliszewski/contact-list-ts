import './PersonInfo.css';

export type Person = {
    id: string;
    firstNameLastName: string;
    jobTitle: string;
    emailAddress: string;
}

type Props = {
  data: Person;
  selected: boolean;
  onClick: () => void;
};

function PersonInfo({ data, selected, onClick }: Props) {
  const initials = data.firstNameLastName.split(' ').map((item) => item[0]).join('');

  return (

    <button
      type="button"
      onClick={onClick}
      className={`person-info ${selected && 'person-info--selected'}`}
      data-testid="person-info"
      aria-label={selected ? 'Unselect' : 'Select'}
    >
      <div className="person-info__initials">{initials}</div>
      <div>
        <div className="person-info__first-name-last-name">{data.firstNameLastName}</div>
        <div className="person-info__job-title">{data.jobTitle}</div>
        <div className="person-info__email-address">{data.emailAddress}</div>
      </div>

    </button>
  );
}

export default PersonInfo;
